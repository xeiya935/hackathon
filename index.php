<?php 
	require "templates/template.php";
	function get_content(){
	require "controllers/connection.php";

		$codes_query = "SELECT name,syntax,codelanguage_id FROM codes";
		$codes = mysqli_query($conn, $codes_query);
		
?>
	<div class="container">
		<div class="row">
			<div class="container-fluid d-flex my-2 mx-1">
				<div class="col-lg-2 border" >
					<!-- side bar -->



		 			<div class="col-lg-2">
		 				<h3 class="text-center py-3">Languages</h3>
		 				<ul class="list-group">
		 					<li class="list-group-item px-5"><a href="code-database.php">All</a></li>
		 					<?php 
		 					//REVIEW THIS LATER
		 					//session_start();

		 					$codelanguages_query = "SELECT * FROM codelanguages";
		 					$codelanguagesList = mysqli_query($conn,$codelanguages_query);

		 					foreach ($codelanguagesList as $indiv_codelanguages){
		 					 ?>
		 					 	<li class="list-group-item px-5">
		 					 		<a href="code-database.php?_id=<?php echo $indiv_codelanguages['id']; ?>">
		 					 			<?php echo $indiv_codelanguages['name']; ?>
		 					 		</a>
		 					 	</li>
		 					<?php
		 					}
		 					 ?>
		 				</ul>
		 			</div>
				</div>
				<div class="col-lg-10 mx-1">
					<!-- <div class="col-lg-12 border" style="height: 120px;"></div> -->
					<div class="col-lg-12 border" style="height: 100%;">
						<table class="table table-striped">
							<thead>
				 				<th>Code</th>
				 				<th>Language</th>
				 				<th>Syntax</th>
				 			</thead>
				 			<tbody id="code_details">
				 				<?php 
				 					foreach($codes as $indiv_code){
				 					 ?>
				 					 	<tr>
				 					 		<td><?php echo $indiv_code['name']; ?></td>
				 					 		<td><?php echo $indiv_code['codelanguage_id']; ?></td>
				 					 		<td><?php echo $indiv_code['syntax']; ?></td>
				 					 	</tr>
				 					<?php
				 					}
				 				 ?>
				 			</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	}

 ?>
