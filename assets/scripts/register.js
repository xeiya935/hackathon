function validate_reg_form(){
	errors = 0;

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let email = document.querySelector("#email").value;
	let password = document.querySelector("#password").value;
	let confirm = document.querySelector("#confirm").value;

	// No empty field
	// password must be at least 8 characters long
	// confirm = password
	if(firstName == ""){
		document.querySelector("#firstName").nextElementSibling.innerHTML= "Please provide a valid First Name";
		errors++;
	}else{
		document.querySelector("#firstName").nextElementSibling.innerHTML= "";
	}

	if(lastName == ""){
		document.querySelector("#lastName").nextElementSibling.innerHTML= "Please provide a valid Last Name";
		errors++;
	}else{
		document.querySelector("#lastName").nextElementSibling.innerHTML= "";
	};

	if(email == ""){
		document.querySelector("#email").nextElementSibling.innerHTML= "Please provide a valid Email";
		errors++;
	}else{
		document.querySelector("#email").nextElementSibling.innerHTML= "";
	}
	;

	if(password == ""){
		document.querySelector("#password").nextElementSibling.innerHTML= "Please provide a valid First Name";
		errors++;
	}else{
		document.querySelector("#password").nextElementSibling.innerHTML= "";
	};

	if(confirm == ""){
		document.querySelector("#confirm").nextElementSibling.innerHTML= "Please confirm your password";
		errors++;
	}else{
		document.querySelector("#confirm").nextElementSibling.innerHTML= "";
	};

	if(password.length<8){
		document.querySelector("#password").nextElementSibling.innerHTML= "Please input a valid password";
		errors++
	}else{
		document.querySelector("#password").nextElementSibling.innerHTML= "";
	}

	if(password!=confirm){
		document.querySelector("#confirm").nextElementSibling.innerHTML= "Please input the same password";
		errors++;
	}else{
		document.querySelector("#confirm").nextElementSibling.innerHTML= "";
	}

	if (errors> 0){
		return false
	}else{
		return true;
	}
	
}

document.querySelector('#registerUser').addEventListener("click", ()=> {
		if(validate_reg_form()){
		let firstName = document.querySelector("#firstName").value;
		let lastName = document.querySelector("#lastName").value;
		let email = document.querySelector("#email").value;
		let password = document.querySelector("#password").value;
	
		let data = new FormData;
	
		data.append("firstName", firstName);
		data.append("lastName", lastName);
		data.append("email", email);
		data.append("password", password);
	
		fetch("../../controllers/process_register_user.php", {
			method: "POST",
			body: data
		}).then(response=>{
			return response.text();
		}).then(data_from_fetch=>{
			console.log(data_from_fetch);
			if(data_from_fetch=="user_exists"){
				document.querySelector("#email").nextElementSibling.innerHTML = "User already exists";
			} else{
				window.location.replace("../../views/login.php");
			}
		})
	}
})