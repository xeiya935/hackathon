//add an eventlistener to the button
loginUser.addEventListener("click", ()=> {
//capture the details
    let email = document.querySelector("#email").value;
    let password = document.querySelector("#password").value;
//create a new formdata
    let data = new FormData;
//append the data
    data.append("email", email);
    data.append("password", password);
//use fetch to access our process_authenticate.php
    fetch("../../controllers/process_authenticate.php", {
        method: "POST",
        body: data
        }).then(response=>{
            return response.text();
        }).then(data_from_fetch=>{
            if (data_from_fetch == "login failed!") {
                document.querySelector("#email").nextElementSibling.innerHTML = "Please provide valid email";
            }else{
                window.location.replace("../../views/code-database.php");
            }
        });
    }
)