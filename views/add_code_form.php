<?php 
	require "../templates/template.php";
	function get_content(){
	require "../controllers/connection.php";
?>
	<h1 class="text-center py-5">ADD FORM</h1>

	<div class="container">
 		<div class="col-lg-6 offset-lg-3">
 			<form action="../controllers/process_add_code.php" method="POST" enctype="multipart/form-data">
 				<div class="form-group">
 					<label for="code">Code: </label>
 					<input type="text" name="code" class="form-control">
 				</div>
 				<div class="form-group">
 					<label for="language">Languages:</label>
 					<input type="text" name="language" class="form-control"></input>
 				</div>
 				<div class="form-group">
 					<label for="syntax">Syntax:</label>
 					<input type="text" name="syntax" class="form-control">
 				</div>
 				<button type="submit" class="btn btn-success">Add Item</button>
 			</form>
 		</div>
 	</div>
<?php
	}
?>