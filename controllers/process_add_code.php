<?php 
	require "connection.php";

	function validate_form(){
		$errors = 0;
		// validation logic
		// we'll check if each of the fields in the form has a value. if, not it will increase the $error total and if the $error total > 0, it will return false.
		// we'll check if the file extension of the image is within the acceptable file extensions. if, not it will increase the $error total and if the $error total > 0, it will return false.

		if($_POST['code']=="" || !isset($_POST['code'])){
			$errors++;
		}

		if($_POST['language']<=0 || !isset($_POST['language'])){
			$errors++;	
		}

		if($_POST['syntax']=="" || !isset($_POST['syntax'])){
			$errors++;	
		}

		if($errors>0){
			return false;
		} else {
			return true;
		}
	};

	if(validate_form()){
		// process of saving an item
		// 1. capture all data from form through $_POST OR $_FILES for image
		// 2. move uploaded image file to the assets/images directory
		// 3. create the query
		// 4. use mysqli_query
		// 5. go back to catalog if successful
		// 6. if unsuccessful go back to add_item_form
		$code = $_POST['code'];
		$language = $_POST['language'];
		$syntax = $_POST['syntax'];

		$add_code_query = "INSERT INTO codes (name, syntax, codelanguage_id) VALUES ('$code','$syntax', $language)";

		$new_code = mysqli_query($conn,$add_code_query);

		var_dump($conn);
		var_dump($add_code_query);
		var_dump($new_code);

		header("Location: ../views/code-database.php");
	} else {
		header("Location: ". $_SERVER['HTTP_REFERER']);
	}

 ?>