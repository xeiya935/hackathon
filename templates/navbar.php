<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="#">Codebase</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="../views/code-database.php">Code Database<span class="sr-only">(current)</span></a>
      </li>
      <?php 
        session_start();
        if(isset($_SESSION['user']) && $_SESSION['user']['role_id']==1){
         ?>
          <li class="nav-item">
            <a class="nav-link" href="#">Edit Code Database</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">Hello, <?php //echo $_SESSION['firstName']; ?></a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="../controllers/process_logout.php">Logout</a>
          </li>
        <?php
        }
         ?>
      <li class="nav-item">
        <a class="nav-link" href="../views/add_code_form.php">Add Codes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/register.php">Register</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../views/login.php">Login</a>
      </li>
    </ul>
    <!-- <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Username / Email">
      <input class="form-control mr-sm-2" type="text" placeholder="Password">
      <button class="btn btn-secondary my-2 my-sm-0" type="submit">Login</button>
    </form> -->
  </div>
</nav>